package com.adsi.bikestorebackend.web.rest;

import com.adsi.bikestorebackend.domain.Users;
import com.adsi.bikestorebackend.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    IUserService userService;

    @PostMapping("/users")
    public Users save(@RequestBody Users user){
        return userService.save(user);
    }

    @GetMapping("/users")
    public Iterable<Users> getUsers(){
        return userService.getAll();
    }
}
