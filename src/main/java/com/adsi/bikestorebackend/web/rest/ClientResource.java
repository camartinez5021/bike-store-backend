package com.adsi.bikestorebackend.web.rest;

import com.adsi.bikestorebackend.domain.Client;
import com.adsi.bikestorebackend.domain.Sale;
import com.adsi.bikestorebackend.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ClientResource {

    @Autowired
    IClientService clientService;

    @GetMapping("/clients")
    public Iterable<Client> read(
            @RequestParam(value = "document.equals", required = false) String document,
            @RequestParam(value = "email.equals", required = false) String email) {
        return clientService.read(document, email);
    }

    @GetMapping("/clients/find-by-document")
    public Client findCliendByDocument( @RequestParam(value = "document", required = false) String document) {
        return clientService.findClientByDocumentContains(document);
    }
    @GetMapping("/clients/find/document")
    public List<Sale> findClientByDocumentWithSale(@RequestParam(value = "document", required = false) String document) {
        List<Sale> clientWithSaleDTO = clientService.findClientByDocumentWithSale(document);
        System.out.println("Client "+clientWithSaleDTO);
        return clientWithSaleDTO;
    }

    @PutMapping("/clients")
    public Client update(@RequestBody Client client) {
        return clientService.update(client);
    }

    @PostMapping("/clients")
    public ResponseEntity<Client> create(@RequestBody Client client) {
        return new ResponseEntity(clientService.create(client), HttpStatus.OK);
    }

    @DeleteMapping("/clients/{id}")
    public void delete(@PathVariable Integer id) {
        clientService.delete(id);
    }

    @GetMapping("/clients/search")
    public Iterable<Client> search(@RequestParam(value = "document", required = false) String documentNumber) {
        return clientService.search(documentNumber);
    }


}
