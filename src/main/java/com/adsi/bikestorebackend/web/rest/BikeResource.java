package com.adsi.bikestorebackend.web.rest;

import com.adsi.bikestorebackend.domain.Bike;
import com.adsi.bikestorebackend.service.IBikeService;
import com.adsi.bikestorebackend.service.dto.BikeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BikeResource {
    //guardar y crear una bicicleta
    @Autowired
    IBikeService bikeService;

    @PostMapping("/bikes")
    public ResponseEntity create(@RequestBody BikeDTO bikeDTO) {
        return bikeService.create(bikeDTO);
    }

    // Get - obtener - read
    @GetMapping("/bikes")
    public Page<BikeDTO> read(@RequestParam(value = "pageSize") Integer pageSize,
                           @RequestParam(value = "pageNumber") Integer pageNumber,
                           @RequestParam(value = "sort.dir", required = false) String dir,
                           @RequestParam(value = "sort" ,required = false) String sort
    ) {
        return bikeService.read(pageSize, pageNumber, sort, dir);
    }

    // Put - actualizar - update
    @PutMapping("/bikes")
    public Bike update(@RequestBody Bike bike) {
        return bikeService.update(bike);
    }

    //Delete - borrar - delete
    @DeleteMapping("/bikes/{id}")
    public void delete(@PathVariable Integer id) {
        bikeService.delete(id);
    }

    @GetMapping("/bikes/{id}")
    public Optional<Bike> getById(@PathVariable Integer id){
        return bikeService.getById(id);
    }

    @GetMapping("bikes/search")
    public ResponseEntity search(@RequestParam(value = "serial", required = false) String serial,
                                 @RequestParam(value = "domain", required = false) String model){
        return bikeService.search(serial, model);
    }

    //Get count of bikes
    @GetMapping("/bikes/count-query")
    public Integer countQuery(){
        return bikeService.countQuery();
    }

    @GetMapping("/bikes/get-model/{model}")
    public List<BikeDTO> getAllModelQuery(@PathVariable String model){
        return bikeService.getAllByModelQuery(model);
    }

    @GetMapping("/bikes/get-list")
    public List<BikeDTO> getAllList(){
        return bikeService.readList();
    }


}


