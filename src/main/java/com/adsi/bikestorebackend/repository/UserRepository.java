package com.adsi.bikestorebackend.repository;

import com.adsi.bikestorebackend.domain.Users;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Users, Long> {
}
