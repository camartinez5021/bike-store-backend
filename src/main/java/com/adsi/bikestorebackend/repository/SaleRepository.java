package com.adsi.bikestorebackend.repository;

import com.adsi.bikestorebackend.domain.Sale;
import org.springframework.data.repository.CrudRepository;

public interface SaleRepository extends CrudRepository<Sale, Integer> {
}
