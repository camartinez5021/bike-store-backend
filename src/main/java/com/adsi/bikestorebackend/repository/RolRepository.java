package com.adsi.bikestorebackend.repository;

import com.adsi.bikestorebackend.domain.Rols;
import org.springframework.data.repository.CrudRepository;

public interface RolRepository extends CrudRepository<Rols, Integer> {
}
