package com.adsi.bikestorebackend.repository;

import com.adsi.bikestorebackend.domain.DetailUser;
import org.springframework.data.repository.CrudRepository;

public interface DetailUSerRepository extends CrudRepository<DetailUser, Long> {
}
