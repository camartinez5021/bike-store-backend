package com.adsi.bikestorebackend.service.transformer;

import com.adsi.bikestorebackend.domain.Bike;
import com.adsi.bikestorebackend.service.dto.BikeDTO;

public class BikeTransformer {

    public static BikeDTO getBikeDTOFromBIke(Bike bike){
        if (bike == null){
            return null;
        }

        BikeDTO dto = new BikeDTO();

        //set variables
        dto.setId(bike.getId());
        dto.setModel(bike.getModel());
        dto.setPrice(bike.getPrice());
        dto.setSerial(bike.getSerial());
        dto.setStatus(bike.getStatus());
        dto.setTypeShockAbsorber(bike.getTypeShockAbsorber());

        return dto;
    }

    public static Bike getBikeFromBikeDTO(BikeDTO dto){
        if(dto == null){
            return null;
        }

        Bike bike = new Bike();

        bike.setId(dto.getId());
        bike.setModel(dto.getModel());
        bike.setPrice(dto.getPrice());
        bike.setSerial(dto.getSerial());
        bike.setStatus(dto.getStatus());
        bike.setTypeShockAbsorber(dto.getTypeShockAbsorber());
        return bike;
    }

}
