package com.adsi.bikestorebackend.service.dto;

import com.adsi.bikestorebackend.domain.Bike;
import com.adsi.bikestorebackend.domain.Client;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class SaleDTO {

    @NotNull
    private int id;

    private LocalDateTime dateSale;
    private Bike bike;
    private Client client;
}
