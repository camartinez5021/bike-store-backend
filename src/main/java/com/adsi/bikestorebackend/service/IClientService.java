package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.domain.Client;
import com.adsi.bikestorebackend.domain.Sale;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IClientService {

    public Iterable<Client> read(String document, String email);

    public Client findClientByDocumentContains(String document);

    public Client update(Client client);

    public ResponseEntity<Client> create(Client client);

    public List<Sale> findClientByDocumentWithSale(String document);

    public void delete(Integer id);

    public Iterable<Client> search(String documentNumber);
}
