package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.domain.Users;

public interface IUserService {

    public Users save(Users user);

    public Iterable<Users> getAll();
}
