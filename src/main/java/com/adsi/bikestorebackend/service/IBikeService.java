package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.domain.Bike;
import com.adsi.bikestorebackend.service.dto.BikeDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface IBikeService {

    public ResponseEntity create(BikeDTO bikeDTO);

    // Get - obtener - read
    public Page<BikeDTO> read(Integer pageSize, Integer pageNumber, String sort, String dir);

    // Put - actualizar - update
    public Bike update(Bike bike);

    //Delete - borrar - delete
    public void delete(Integer id);

    public Optional<Bike> getById(Integer id);

    public ResponseEntity search(String serial, String model);

    public Integer countQuery();

    public List<BikeDTO> getAllByModelQuery(String model);

    // Get - obtener - read
    public List<BikeDTO> readList();


}
