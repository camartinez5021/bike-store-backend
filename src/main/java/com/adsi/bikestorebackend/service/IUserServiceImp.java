package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.domain.Users;
import com.adsi.bikestorebackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IUserServiceImp implements IUserService{

    @Autowired
    UserRepository userRepository;

    @Override
    public Users save(Users user) {
        return userRepository.save(user);
    }

    @Override
    public Iterable<Users> getAll() {
        return userRepository.findAll();
    }
}
