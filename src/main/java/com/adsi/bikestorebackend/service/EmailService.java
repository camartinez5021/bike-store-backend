package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.service.dto.SaleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Service
public class EmailService {

    private  static final  String LOGO_URL = "cicle.png";

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmailSale(SaleDTO saleDTO){
        Locale locale = Locale.forLanguageTag("es");
        Context context = new Context(locale);
        context.setVariable("imageResourceName", LOGO_URL);
        context.setVariable("name", saleDTO.getClient().getName());
        context.setVariable("phone", saleDTO.getClient().getPhoneNumber());
        context.setVariable("email", saleDTO.getClient().getEmail());
        context.setVariable("bikeModel", saleDTO.getBike().getModel());
        context.setVariable("bikeSerial", saleDTO.getBike().getSerial());
        context.setVariable("bikePrice", saleDTO.getBike().getPrice());

        String contentForm = templateEngine.process("mail/saleEmail.html", context);
        sendEmail(saleDTO.getClient().getEmail(),
                "Compra realizada por " + saleDTO.getClient().getName(),
                contentForm, true, true);

    }

    public void sendEmail(String to, String subject, String content, boolean isHtml, boolean isMultipart){
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setSubject(subject);
            message.setText(content, isHtml);
            message.addInline(LOGO_URL, new ClassPathResource("/static/img/circle.png"));
            javaMailSender.send(mimeMessage);

        }catch (MessagingException e){
            e.printStackTrace();
        }

    }
}
