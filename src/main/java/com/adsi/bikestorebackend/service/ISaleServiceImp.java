package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.domain.Client;
import com.adsi.bikestorebackend.domain.Sale;
import com.adsi.bikestorebackend.repository.ClientRepository;
import com.adsi.bikestorebackend.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ISaleServiceImp implements ISaleService {


    @Autowired
    SaleRepository saleRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public Sale create(Sale sale) {


        sale.setDateSale(LocalDateTime.now());
        Sale aSale = saleRepository.save(sale);

        //Send the email
       Optional<Client> client = clientRepository.findById(sale.getClient().getId());
        sendEmail(client.get().getEmail(),
                "Nueva compra",
                "Has realizado una nueva compra " + client.get().getName());

        return aSale;
    }
 @Override
    public Sale createList(List<Sale> sale) {

        Sale itemSale = new Sale();
        for (Sale item :sale) {
            item.setDateSale(LocalDateTime.now());
            itemSale = saleRepository.save(item);
        }
   /*     sale.setDateSale(LocalDateTime.now());
        Sale aSale = saleRepository.save(sale);
*/
        //Send the email
    /*    Optional<Client> client = clientRepository.findById(sale.getClient().getId());
        sendEmail(client.get().getEmail(),
                "Nueva compra",
                "Has realizado una nueva compra " + client.get().getName());
        */
        return itemSale;
    }

    @Override
    public Iterable<Sale> read() {
        return saleRepository.findAll();
    }

    @Override
    public Optional<Sale> findById(int id) {
        return saleRepository.findById(id);
    }

    @Override
    public Sale update(Sale sale) {
        return saleRepository.save(sale);
    }

    private void sendEmail(String to, String subject, String text){
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(to);
        msg.setSubject(subject);
        msg.setText(text);

        javaMailSender.send(msg);
    }
}
