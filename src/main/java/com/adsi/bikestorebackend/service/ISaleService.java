package com.adsi.bikestorebackend.service;

import com.adsi.bikestorebackend.domain.Sale;

import java.util.List;
import java.util.Optional;

public interface ISaleService {

    public Sale create(Sale sale);
    public Sale createList(List<Sale> sale);
    public Iterable<Sale> read();

    public Optional<Sale> findById(int id);

    public Sale update(Sale sale);

}


