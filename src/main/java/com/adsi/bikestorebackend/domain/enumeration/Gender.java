package com.adsi.bikestorebackend.domain.enumeration;

public enum Gender {
    MALE, FEMALE, OTHER
}
