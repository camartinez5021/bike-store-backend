package com.adsi.bikestorebackend.domain.enumeration;

public enum TypeShockAbsorber {
    RIGIDA,
    HARDTAIL,
    FULL_SUSPENSION
}
