package com.adsi.bikestorebackend.domain;

import com.adsi.bikestorebackend.domain.enumeration.TypeShockAbsorber;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Bike {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String model;
    private Double price;
    private String serial;
    private Boolean status;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_shock_absorber")
    private TypeShockAbsorber typeShockAbsorber;


    public String getSerial() {
        return serial;
    }
}
