package com.adsi.bikestorebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BikestoreBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BikestoreBackendApplication.class, args);
	}

}
